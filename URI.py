# QUESTÃO 1002
digitar_raio = float(input( ))
n = 3.14159
calcular_area = n * (digitar_raio ** 2)
print("A=""%.4f"%calcular_area)

# QUESTÃO 1003
A = int( input( ) )
B = int( input( ) )
SOMA = A + B
print('SOMA = ',SOMA )

# QUESTÃO 1004
A = int( input( ) )
B = int( input( ) )
PROD = A * B
print('PROD =', PROD )

#QUESTÃO 1005
A = float( input( ) )
B = float( input( ) )
calcular_media =  (  ( ( A * 3.5 ) + ( B * 7.5 ) ) / 11 )
print('MEDIA =',"%.5f" % calcular_media )

#QUESTÃO 1006
A = float(input( ) )
B = float(input( ) )
C = float(input( ) )
media =  ( ( ( A * 2 ) + ( B * 3 ) + ( C * 5 ) ) / 10 )
print('MEDIA =',"%.1f" % media )

x1,y1 = input(  ).split()
x2,y2 = input(  ).split()
x1 = float(x1)
y1 = float(y1)
x2 = float(x2)
y2 = float(y2)
calculo_das_variaveis = (((x2 - x1) ** 2) + ((y2 - y1) ** 2))** 0.5
print("%.4f" % calculo_das_variaveis)
#foi rodado 8 vezes
# LEMBRAR QUE O .SPLIT CONVERTE A VARIAVEL EM STRING 

#QUESTÃO 1017
tempo =  float( input( ) )
velocidade =  float( input( ) )
calculo =  ( tempo * velocidade ) / 12  
print('%.3f'%calculo )

#QUESTÃO 1052
numero = int(input( ) )
lista = ['False', 'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December']     
print(lista.pop(numero))

#QUESTÃO 1074
quantidade = int(input( ) )
for i in range (quantidade):
    incognita = int( input( ) )
    if (incognita > 0 ) and ( incognita % 2 == 0 ) :
        print ('EVEN POSITIVE')
    elif incognita > 0 :
        print('ODD POSITIVE')
    elif ( incognita < 0) and ( incognita % -2 == 0):
        print('EVEN NEGATIVE')
    elif incognita < 0 :
        print('ODD NEGATIVE')
    else:
        print('NULL')